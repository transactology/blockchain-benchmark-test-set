BBTS: Blockchain Benchmark Test Set
====

## Introduction

BBTS aims to benchmark a pragmatic performance of blockchain architectures.

## Focus

### Test Set

BBTS does not provides benchmark framework, but provides only test set.  
BBTS is consisted with smart contract and sequential data.  
When a benchmark tool does not provides feature which can test with our data sequentially, BBTS may provide plug-in for the benchmark tool.

### Validator

Unfortunately, distributed and transactional system could not process the transactions by fixed order.  
Therefore, you can not check the integrity of the result by just comparing the answer.

You have to re-calculate the result from the recorded transaction order.

### Analyzer

From the signals and results on the ledger,  
we provides more detailed and deep analysis results.  
For example, the transaction processing delay is categorized as  
systematical, architectural, etc.
